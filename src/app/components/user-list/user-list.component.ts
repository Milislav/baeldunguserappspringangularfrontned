import { Component, OnInit } from '@angular/core';
import {User} from "../../classes/user";
import {UserServiceService} from "../../services/user-service.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[] | undefined;

  constructor(public userService : UserServiceService ) { }

  ngOnInit(): void {
    this.userService.findAll().subscribe(value => {
      this.users = value
    });
  }

}
