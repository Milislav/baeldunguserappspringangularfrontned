import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../classes/user";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http : HttpClient) { }

  public findAll(): Observable<User[]>{

    return this.http.get<User[]>(environment.userUrl)
  }

  public addUser(user : User){
    return this.http.post<User>(environment.userPostUrl , user);
  }

}
